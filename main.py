#!/usr/bin/env python

import tkinter as tk

from gui.main import GkTimerGUI

root = tk.Tk()  # Make temporary window for app to start
root.withdraw()  # WithDraw the window


if __name__ == "__main__":

    root.title('gkTimer')

    GkTimerGUI()
    root.mainloop()
