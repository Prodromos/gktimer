# GK Timer Project

A simple timer project created for measuring the solving speed of the Rubik's Cube
for the needs of a Centre of Creative Activities for Children in the city of Kavala, Greece.

It was used during a competition among the children.