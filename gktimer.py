# /usr/bin/python3
import time
import keyboard
from threading import Thread

gk_time = None
done = False

ms_time_task = None
check_exit_task = None


def check_exit():
    global gk_time
    global done

    while True:
        if keyboard.is_pressed('Q') or keyboard.is_pressed('q'):
            done = True
            break


def ms_timer():
    global gk_time
    global check_exit_task
    global done

    done = False
    gk_time = '0'

    while True:
        if keyboard.is_pressed(' '):
            break

    check_exit_task.start()

    start_ns = time.time_ns()

    while done is False:
        current_ms = (time.time_ns() - start_ns) // 1000000

        ms = current_ms % 1000

        tmp_secs = current_ms // 1000

        minutes = tmp_secs // 60

        seconds = tmp_secs - (minutes * 60)

        gk_time = str('{:02d}:{:02d}:{:03d}\r'.format(minutes, seconds, ms))

        # Sleep for about 10ms (not accurate but useful for saving computational resources
        time.sleep(0.01)


def gk_timer_get():
    global gk_time
    return gk_time


def gk_timer_is_done():
    global done
    return done


def gk_timer_init():
    global ms_time_task
    global check_exit_task

    ms_time_task = Thread(target=ms_timer)
    check_exit_task = Thread(target=check_exit)

    ms_time_task.start()
