#!/usr/bin/env python
import time
from pathlib import Path
from threading import Thread
from tkinter import (
    Canvas,
    Button,
    PhotoImage,
    Toplevel)

import gktimer

OUTPUT_PATH = Path(__file__).parent
ASSETS_PATH = OUTPUT_PATH / Path("./assets")


def relative_to_assets(path: str) -> Path:
    return ASSETS_PATH / Path(path)


class GkTimerGUI(Toplevel):
    def __init__(self, *args, **kwargs):
        Toplevel.__init__(self, *args, **kwargs)
        self.protocol("WM_DELETE_WINDOW", self.on_closing)
        self.geometry("1000x500")
        self.resizable(False, False)
        self.configure(bg="#FFFFFF")

        self.time_text = None

        self.min_time = 0
        self.max_time = 0
        self.mean_time = 0
        self.results = list()

        self.canvas = Canvas(
            self,
            bg="#FFFFFF",
            height=500,
            width=1000,
            bd=0,
            highlightthickness=0,
            relief="ridge",
        )

        self.canvas.place(x=0, y=0)

        enc_img = PhotoImage(file=relative_to_assets("rubik2.png"))

        self.canvas.create_image(
            380, 50, image=enc_img, anchor="nw")
        self.canvas.image = enc_img

        self.canvas.create_rectangle(
            40.0, 14.0, 942.0, 16.0, fill="#EFEFEF", outline=""
        )

        self.canvas.create_rectangle(
            40.0, 342.0, 942.0, 344.0, fill="#EFEFEF", outline=""
        )

        self.canvas.create_rectangle(
            750.0, 15.0, 753.0, 344.0, fill="#EFEFEF", outline=""
        )

        self.canvas.create_text(
            116.0,
            33.0,
            anchor="nw",
            text="GkTimer Project",
            fill="#5E95FF",
            font=("Montserrat Bold", 26 * -1),
        )

        self.canvas.create_text(
            116.0,
            65.0,
            anchor="nw",
            text="Measure your Rubik's Cube time",
            fill="#808080",
            font=("Montserrat SemiBold", 16 * -1),
        )

        self.canvas.create_text(
            40.0,
            367.0,
            anchor="nw",
            text="Select Action:",
            fill="#5E95FF",
            font=("Montserrat Bold", 26 * -1),
        )

        self.canvas.create_text(
            180.0,
            250.0,
            anchor="nw",
            text="Time:",
            fill="#808080",
            font=("Montserrat SemiBold", 25 * -1),
        )

        self.time_text = self.canvas.create_text(
            320.0,
            249.0,
            anchor="nw",
            text='START, then SPACE. Stop with Q',
            fill="#808085",
            font=("Montserrat SemiBold", 25 * -1),
        )

        self.text_results = self.canvas.create_text(
            800.0,
            40.0,
            anchor="nw",
            text='Results',
            fill="#808085",
            font=("Montserrat SemiBold", 22 * -1),
        )

        # Add sample data
        self.select_btn_img = PhotoImage(file=relative_to_assets("start.png"))
        self.start_btn = Button(
            self,
            image=self.select_btn_img,
            borderwidth=0,
            highlightthickness=0,
            command=self.handle_start_cb,
            relief="flat",
            state="normal",
        )

        self.start_btn.place(x=302.0, y=359.0, width=121.0, height=100.0)

    def on_closing(self):
        self.destroy()
        self.quit()

    def refresh_time(self):
        gktime_str = None

        while gktimer.done is False:
            gktime_str = str(gktimer.gk_timer_get())
            self.canvas.itemconfig(self.time_text, text=gktime_str)
            time.sleep(0.1)

        self.results.append(gktime_str)

        text_results = ''
        i = 1
        for s in self.results:
            if i >= 6:
                self.results.remove(self.results[0])

            text_results += str(i) + ': ' + str(s) + '\n'
            i += 1

        self.canvas.itemconfig(self.text_results, text=text_results)


    def handle_start_cb(self):
        gktimer.gk_timer_init()

        self.canvas.itemconfig(self.time_text, text='0')

        refresh_time_task = Thread(target=self.refresh_time)
        refresh_time_task.start()
